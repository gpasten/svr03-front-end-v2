

declare global {
    interface Window{
        URL_KEYCLOAK : string,
        REALM : string,
        CLIENT  : string
    }

}

export const environmet = {
    production: false, 
    CLIENT: `${window.CLIENT}`, 
    REALM:  `${window.REALM}`,
    URL_KEYCLOAK: `${window.URL_KEYCLOAK}`


};